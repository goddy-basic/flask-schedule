# -*- coding: utf-8 -*-
'''
 @File: config
 @Author: Goddy
 @Contact: wuchuansheng@yeah.net
 @Date: 2018/5/29 下午7:19
 @Desc: 配置文件
'''


'''Environment'''

# development
# env = 'DevConfig'

# production
# env = 'ProConfig'

def set_env(environment):
    '''
    配置环境变量
    :param environment:
    :return:
    '''
    global env
    env = environment


'''定时任务'''

class Config(object):

    SCHEDULER_API_ENABLED = True

class DevConfig(Config):
    JOBS = [
        {
            'id': 'job3',
            'func': 'schedule_job:insert_test',
            'args': '',
            'trigger': {
                'type': 'interval',
                'seconds': 5
            }
        },
        {
            'id': 'job1',
            'func': 'schedule_job:test_data',
            'args': '',
            'trigger': {
                'type': 'cron',
                'day_of_week': "mon-fri",
                'hour': '0-23',
                'minute': '0-59',
                'second': '*/2'
            }
        }, {
            'id': 'job2',
            'func': 'schedule_job:test_boom',
            'args': '',
            'trigger': {
                'type': 'interval',
                'seconds': 5
            }
        }
    ]

class ProConfig(Config):
    JOBS = [
        {
            'id': 'job3',
            'func': 'schedule_job:insert_test',
            'args': '',
            'trigger': {
                'type': 'interval',
                'seconds': 5
            }
        }
    ]

def mysql_config():
    if env == 'DevConfig':
        return {
            'host': '127.0.0.1',
            'user': 'root',
            'password': 'xxxx',
            'db': 'xxxx',
            'port': 3306,
            'charset': 'utf8'
        }
    elif env == 'ProConfig':
        return {
            'host': 'xxx',
            'user': 'root',
            'password': 'xxxx',
            'db': 'xxxx',
            'port': 3306,
            'charset': 'utf8'
        }
    else:
        raise RuntimeError('no such config ...')