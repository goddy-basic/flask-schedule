# -*- coding: utf-8 -*-
'''
 @File: mysqlUtil
 @Author: Goddy
 @Contact: wuchuansheng@yeah.net
 @Date: 2018/5/31 下午6:54
 @Desc: 
'''
import pandas as pd
import pymysql
from config import mysql_config

def conn_mysql():
    '''
    根据环境连接数据库
    :return: mysql配置
    '''
    config = mysql_config()

    return pymysql.connect(
        host=config['host'],
        user=config['user'],
        password=config['password'],
        db=config['db'],
        port=config['port'],
        charset=config['charset']
    )

def read(sql):
    '''
    读操作
    :param sql: sql语句
    :return: 读取后数据
    '''
    print("【mysql】'{}'".format(sql))
    return pd.read_sql(sql, conn_mysql())

def save(sql):
    '''
    添加/更新操作
    :param sql:
    :return:
    '''
    print("【mysql】'{}".format(sql))
    conn = conn_mysql()
    conn.cursor().execute(sql)
    conn.commit()