# -*- coding: utf-8 -*-
'''
 @File: app.py
 @Author: Goddy
 @Contact: wuchuansheng@yeah.net
 @Date: 2018/5/29 下午6:39
 @Desc: 启动文件
'''

import sys
from flask_apscheduler import APScheduler
from flask import Flask
import schedule_job
import config

app = Flask(__name__)


@app.route("/")
def hello():
    return "hello world"

@app.route("/test")
def test():
    return "success"

@app.route("/insert")
def insert():
    schedule_job.insert_test()
    return "success"


if __name__ == '__main__':
    scheduler = APScheduler()

    env = sys.argv[1] if len(sys.argv) > 1 else 'DevConfig'
    print("Python program begins in %s environment." % (env))

    config.set_env(env)

    # app.config.from_object('config.' + config.env)
    app.config.from_object('config.' + env)

    scheduler.init_app(app)
    scheduler.start()

    app.run(host='0.0.0.0', port=5000, debug=False)
