FROM python
ADD . /code
WORKDIR /code
RUN pip install -r requirements.txt
RUN ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
CMD ["python", "app.py", "ProConfig"]
