# -*- coding: utf-8 -*-
'''
 @File: schedule_job
 @Author: Goddy
 @Contact: wuchuansheng@yeah.net
 @Date: 2018/4/18 下午10:49
 @Desc: 定时任务
'''
import datetime
from task1 import task1_insert

def test_data():
    print("I am working:%s" % (datetime.datetime.now()))

def test_boom():
    print("BOOM !!!")

def insert_test():
    task1_insert.insert()
    print('inserted!')