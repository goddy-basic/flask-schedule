# 定时任务 + 接口

## 技术选型
* flask
* flask_apscheduler

## 切换环境
```
自动切换
```

## 架构
```
#数据库处理层
1.DAO
1)provider_dao.py

#示例项目
2.task1
1)task1_insert.py

#工具包
3.util
1)mysqlUtil.py

#配置文件
4.config.py

#启动文件 + 接口路由
5.app.py

#定时任务启动方法汇总
6.schedule_job.py
```